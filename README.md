# Readme.md

This repository contains the source code for Łukasz Tumiel's personal website. The website serves as a portfolio and provides information about Łukasz's background, skills, and experience as a software tester.

## Technologies Used

The website is built using the following technologies:

- HTML
- CSS
- JavaScript

## Dependencies

The project utilizes the following external dependencies:

- [boxicons](https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css) - CSS icon library

## Getting Started

To run the website locally, follow these steps:

1. Clone this repository:

   ```shell
   git clone gitlab.com/rid0x/your-repository.git
   ```

2. Open the cloned directory:

   ```shell
   cd your-repository
   ```

3. Open the `index.html` file in your preferred web browser.

## Directory Structure

The repository has the following structure:

```
├── css/
│   └── style.css
├── favicon/
│   ├── apple-touch-icon.png
│   ├── favicon-32x32.png
│   └── favicon-16x16.png
├── images/
│   └── imgphoto2.jpg
├── index.html
└── readme.md
```

- The `css/` directory contains the CSS file for styling the website.
- The `favicon/` directory contains the favicon images used in the website.
- The `images/` directory contains the profile picture used on the website.
- The `index.html` file is the main HTML file that defines the structure and content of the website.
- The `readme.md` file is the documentation file you are currently reading.

## About Łukasz Tumiel

Łukasz Tumiel is a Junior Software Tester with a background in sales and export management. He is currently focusing on starting a new career in software testing. The website provides information about his skills, education, experience, and interests.

## Contact

To contact Łukasz Tumiel, you can use the following methods:

- GitHub: [rid0x](https://github.com/rid0x)
- LinkedIn: [Łukasz Tumiel](https://www.linkedin.com/in/%C5%82ukasz-tumiel-89965a1b6/)
- GitLab: [rid0x](https://gitlab.com/rid0x)
- Instagram: [numtralabum](https://www.instagram.com/numtralabum/)
- Phone: [510245267]
- Email: [lukasztumiel@gmail.com]
